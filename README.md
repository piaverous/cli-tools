# Simple command line utilities

Making a couple CLI utilities for myself.

## DUC - File size analysis

How do they perform ? Let's compare the different implementations on my `$HOME` folder using the `time` command.
```
$ du -shc * .* | sort -hr 
1,41s user 29,21s system 45% cpu 1:06,82 total

$ duc (my RustLang implementation)
5,08s  user 50,72s system 62% cpu 1:29,79 total

$ duc.py (my Python implementation)
17,23s user 62,78s system 62% cpu 2:07,69 total
```

## TRT - Retry a command multiple times

Having timeout issues I snapped and created this small utility, that retries them 10 times until they do work. 

```
$ trt ls
Starting with 10 retries for command "ls"
README.md       duc             env             trt

Exiting after 1 retries
0 minutes and 0 seconds elapsed.
```

## REMTML - Convert HTML table to CSV



## DKCL - Docker Clean

Clean up those docker containers and images that you have scattered around your computer

```
$ dkcl rmi timekeeper
Untagged: timekeeper:latest
Deleted: sha256:8732e9722d9d561104ffdbc96831e86204fa1d953989640c1c17b9d7bc51ae04
Deleted: sha256:8b5e79471dc7e78c11d8d2b6cb343911c4a9a9da562d6f0a7f3cb3bc559efd76
Deleted: sha256:5deffdb2715ec73f8a2337499f61aae4d86deee181c0d3a159fb9513f59e4038
Deleted: sha256:01484ce821f9199439a62bf7ddab7071b3f3dbac4c3c850ae710b0863bbbbd4f
Deleted: sha256:e10924dbb1367b528dc5bf16170e8d206e01068d28199831552a34b31c3a8f29
Deleted: sha256:edfa761452ba329f4af31e73d041b19ac533a7f5eefc7c72df6832a38173a47b
Deleted: sha256:de27097fbb14f454e11cb89e0a5cc3f40c33566701419658982eb26da5cd1cb5
Deleted: sha256:178e3e1d083cadce905a8dc0461148162bcbfe05dfee56faed139244459e0add
```