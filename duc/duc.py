#!/Users/piaverous/anaconda3/bin/python
""" Handy script to get the size of the contents of a directory 

Looks better than du... But is slower :("""

import os
from tqdm import tqdm
import bisect

KILO = 1024
MEGA = 1048576
GIGA = 1073741824

def get_size(start_path = '.'):
    walk = os.walk
    join, getsize, islink = os.path.join, os.path.getsize, os.path.islink

    total_size = 0
    for dirpath, _, filenames in walk(start_path):
        for f in filenames:
            fp = join(dirpath, f)
            # skip if it is symbolic link
            if not islink(fp):
                total_size += getsize(fp)

    return total_size

def format_size(size, unit="MB"):
    bit_shift = {
        "B": 0,
        "kB": 10,
        "MB": 20,
        "GB": 30,
        "TB": 40
    }
    return "{:,.2f}".format(size / float(1 << bit_shift[unit])) + " " + unit

def human_file_size(size):
    if size > GIGA:
        return format_size(size, unit="GB")
    if size >= MEGA:
        return format_size(size, unit="MB")
    if size >= KILO:
        return format_size(size, unit="kB")
    return format_size(size, unit="B")


def as_string(seq_of_rows, max_length):
    return '\n'.join(''.join([ row[1].center(max_length), human_file_size(-row[0]) ]) for row in seq_of_rows)

def main():
    sum, max_length =0, 30
    file_list = os.listdir('./')

    files = []
    for filename in tqdm(file_list, desc="Scanning files"):
        size = 0
        if os.path.isfile(filename):
            size = os.path.getsize(filename)
        else:
            size = get_size(filename)
        sum += size
        
        l = len(filename)+5
        max_length = l if l > max_length else max_length

        bisect.insort(files, (-size, filename))
        
    print()
    print( ''.join([ "Name".center(max_length), 'Size' ]) )
    print( ''.join(["-" for i in range(max_length+12)]) )

    print( as_string(files, max_length) )
    print()
    print( ''.join([ "TOTAL".center(max_length), human_file_size(sum) ]) )


if __name__ == "__main__":
    main()