extern crate unbytify;
extern crate indicatif;

use std::io;
use std::fs::{self, DirEntry};
use std::path::Path;
use unbytify::*;
use indicatif::ProgressBar;

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct FileSize {
    name: String,
    size: u64
}

impl FileSize {
    pub fn new(name: String, size: u64) -> Self {
        FileSize {
            name,
            size
        }
    }
}

// one possible implementation of walking a directory only visiting files
fn visit_dirs(dir: &Path, cb: &dyn Fn(&DirEntry) -> u64) -> io::Result<u64> {
    let mut size = 0;
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                size = size + visit_dirs(&path, cb)?;
            } else {
                size = size + cb(&entry);
            }
        }
    }
    Ok(size)
}

fn get_file_size(entry: &DirEntry) -> u64 {
    if let Ok(metadata) = entry.metadata() {
        return metadata.len();
    } 
    return 0;
}

fn main() -> io::Result<()> {
    let entries = fs::read_dir(".")?;
    let count = fs::read_dir(".")?.count() as u64;
    let bar = ProgressBar::new( count );
    let mut stack = Vec::new();

    for entry in entries {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            let visit_result = visit_dirs(&path, &get_file_size);
            match visit_result {
                Ok(size) => {
                    stack.push(FileSize::new(String::from(path.to_str().unwrap()), size));
                },
                Err(error) => {
                    println!("Error for path {:?} : {}", path, error);
                },
            };
        } else {
            let size = get_file_size(&entry);
            stack.push(FileSize::new(String::from(path.to_str().unwrap()), size));
        }
        bar.inc(1);
    }
    bar.finish();

    stack.sort_by(|a, b| b.size.cmp(&a.size));

    println!("{:^40}   {:<8} {}", "FILE NAME", "SIZE", "UNIT");
    println!("--------------------------------------------------------");
    for file in &stack {
        let fmt_size = bytify(file.size);
        println!("{:^40} - {:<8} {}", file.name, fmt_size.0, fmt_size.1);
    }

    Ok(())
}
